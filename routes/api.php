<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('file', function(){
    $contents = Storage::get('image.png');
    $fileName = \Str::uuid();
    $headers = [
        'Content-Type'        => 'Content-Type: application/png',
        'Content-Disposition' => "attachment; filename={$fileName}.png",
    ];
    return \Response::make($contents, 200, $headers);
});